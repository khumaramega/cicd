const { barang, pelanggan, transaksi } = require('../../models')
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params
module.exports = {
  create: [
    //Set form validation rule
    check('nama').isString().custom(value => {
      return pemasok.findById(value).then(b => {
        if (!b) {
          throw new Error(' nama pemasok harus string!');
        }
      })
    }),

  ],
  update: [
    //Set form validation rule
    check('nama').custom(value => {
      return pemasok.findById(value).then(b => {
        if (!b) {
          throw new Error('nama pemasok gagal ditambahkan!');
        }
      })
    }),
    
  ],
};

